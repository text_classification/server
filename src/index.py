import json
import os
import re
import string
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from tensorflow import keras
from flask import Flask
import flask
from flask import jsonify

# from keras.preprocessing.sequence import pad_sequences

from modules.convertStringToNumber import convertStringToNumber
from modules.padding import padding
from modules.preProcess import preProcess

__dirname = os.path.dirname(os.path.abspath(__file__))
def makePath(path:str)->str:
    return os.path.realpath(os.path.join(__dirname, path))

# label_names = ['tào lao', 'ngon - bình thường', 'dở', 'giá cao', 'giá hợp lý', 
#                'vệ sinh sạch sẽ - thực phẩm an toàn', 'vệ sinh bẩn - thực phẩm không đảm bảo', 
#                'thái độ phục vụ tốt', 'thái độ phục vụ tệ']



label_names = {
    1: 'ngon - bình thường',
    2: 'dở',
    3: 'giá cao',
    4: 'giá hợp lý',
    5: 'vệ sinh sạch sẽ - thực phẩm an toàn',
    6: 'vệ sinh bẩn - thực phẩm không đảm bảo',
    7: 'thái độ phục vụ tốt',
    8: 'thái độ phục vụ tệ',
    9: 'tào lao',
}

model = keras.models.load_model(makePath('../data/model.h5'))

MAX_LENGTH = 120

def predict(text:str):
    text = preProcess(text)
    text = padding(text, MAX_LENGTH)
    text = convertStringToNumber(text)
    text = np.array([text])
    text = pad_sequences(text, maxlen=MAX_LENGTH, padding='post')
    text = np.array(text).astype("float32")

    index = int(np.argmax(model.predict(text)) + 1)
    classes = label_names[index]
    return classes


app = Flask(__name__)

@app.route('/predict/<v>', methods=['GET'])
def defaultRouter(v:str):
    # data = json.loads(flask.request.data)
    # v = data['value']
    result = predict(v)

    print(v)
    print(result)

    responseData = {
        'result': result
    }

    res = flask.make_response(jsonify(responseData), 200)
    res.headers.add('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    res.headers.add('Access-Control-Allow-Origin', '*')

    return res

# Change port
def main():
    app.run(host='0.0.0.0', port=8080, debug=True)

main()