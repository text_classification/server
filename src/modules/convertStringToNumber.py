import json
import os

__dirname = os.path.dirname(os.path.abspath(__file__))
def makePath(path:str)->str:
    return os.path.realpath(os.path.join(__dirname, path))

path_of_wordtoid =makePath('../../data/wordtoid.json')

f = open(path_of_wordtoid, 'r')
data = f.read()
f.close()

wordtoid = json.loads(data)

def convertStringToNumber(sent):
    return [wordtoid[i] if i in list(wordtoid) else 0 for i in str(sent).split()]

# print(convertStringToNumber('nhân viên phục vụ'))