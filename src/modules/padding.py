MAX_LENGTH = 200

def padding(v:str, maxLength:int = MAX_LENGTH)->str:
    length = len(str(v).split())

    tmp = maxLength - length
    if tmp <= 0:
        return v
    
    v = v + ' Pad'*tmp
    return v