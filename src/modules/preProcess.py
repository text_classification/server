import string
from underthesea import word_tokenize

print('preProcess loaded')

def preProcess(comment:str)->str:
    table = str.maketrans('','',string.punctuation)
    comment =  str(comment).translate(table)
    s = str()
    for word in word_tokenize(comment):
        word = word.replace(' ','_')
        s = s.replace('/','')
        s = s.replace('!','')
        s = s.replace('?','')
        s = s.replace('@','')
        word = word.lower()
        s = s + ' ' + word
        s = s.strip()
    comment = s
    return comment